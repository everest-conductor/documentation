Getting Started - Install
=========================

.. toctree::
   :maxdepth: 1
   :hidden:

   prereqs
   install


The Everest Conductor integrates different components:

.. image:: ./images/whonome-logo.png

* :doc:`Prerequisite Software <prereqs>`: the required software needed to run Everest Conductor.
* :doc:`Everest Conductor <install>`: the Everest Conductor dockers to run the Framework.


