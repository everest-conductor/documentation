# Prerequisites

The following prerequisites are required to run Everest Conductor Framework.

## **Linux (Ubuntu 22.04)**

<div style="margin-left: 1.5em;">

### Docker

Set up Docker's apt repository.

```shell
# Add Docker's official GPG key:
sudo apt-get update
sudo apt-get install ca-certificates curl gnupg
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg

# Add the repository to Apt sources:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
```
Install the latest version
```shell
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```

### PostgreSQL and PGroonga

You can use the following commands to install PGroonga for system PostgreSQL:

```shell
sudo apt install -y software-properties-common
sudo add-apt-repository -y universe
sudo add-apt-repository -y ppa:groonga/ppa
sudo apt install -y wget lsb-release
wget https://packages.groonga.org/ubuntu/groonga-apt-source-latest-$(lsb_release --codename --short).deb
sudo apt install -y -V ./groonga-apt-source-latest-$(lsb_release --codename --short).deb
echo "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release --codename --short)-pgdg main" | sudo tee /etc/apt/sources.list.d/pgdg.list
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo apt update
```

Installing PostgreSQL
```shell
sudo apt install postgresql
```
Installing PGroonga
```shell
sudo apt install -y -V postgresql-16-pgdg-pgroonga
```

### OpenJDK

Installing OpenJDK 17
```shell
sudo apt install openjdk-17-jdk
```

### OpenLDAP

Installing OpenLDAP
```shell
sudo apt-get install slapd ldap-utils
```
Configure OpenLDAP
```shell
sudo dpkg-reconfigure slapd
```
Answer a series of questions as following:
- Omit OpenLDAP server configuration? No
- The DNS domain name is used to construct the base DN of the LDAP directory. For example, 'foo.example.org' will create the directory with 'dc=foo, dc=example, dc=org' as base DN. Provide your desired domain name like example.com.
- Organization name: Provide your organization name
- Administrator password: 
- Confirm administrator password:
- Do you want the database to be removed when slapd is purged? No
- Move old database? Yes

Create self-signed SSL Certificates for the OpenLDAP server
```shell
sudo -i
cd /etc/ssl/private
openssl genrsa -aes128 -out server.key 2048 
openssl rsa -in server.key -out server.key
openssl req -new -days 3650 -key server.key -out server.csr
openssl x509 -in server.csr -out server.crt -req -signkey server.key -days 3650     

cp /etc/ssl/private/server.key \
/etc/ssl/private/server.crt \
/etc/ssl/certs/ca-certificates.crt \
/etc/ldap/sasl2/

chown openldap. /etc/ldap/sasl2/server.key \
/etc/ldap/sasl2/server.crt \
/etc/ldap/sasl2/ca-certificates.crt
```

Configure OpenLDAP Server to support SSL.

Edit /etc/ldap/ldap.conf and add the following:
```shell
TLS_REQCERT allow
ssl start_tls
ssl on
```

Create file a file named modify_ssl.ldif with the following contents:
```shell
# modify OpenLDAP to support SSL
dn: cn=config
changetype: modify
add: olcTLSCACertificateFile
olcTLSCACertificateFile: /etc/ldap/sasl2/ca-certificates.crt
-
replace: olcTLSCertificateFile
olcTLSCertificateFile: /etc/ldap/sasl2/server.crt
-
replace: olcTLSCertificateKeyFile
olcTLSCertificateKeyFile: /etc/ldap/sasl2/server.key
```

Run the following command:
```shell
ldapmodify -Y EXTERNAL -H ldapi:/// -f modify_ssl.ldif
```

Restart slapd
```shell
sudo service slapd restart
```

Verify the secure connection to OpenLDAP server
```shell
ldapwhoami -H ldap://localhost -x -ZZ
```

###Create Admin Group
Create a file named ecadmingroup.ldif with the following contents:
```shell
dn: cn=Admin,ou=Groups,dc=whonome,dc=com
cn: Admin
gidNumber: 5000
objectClass: posixGroup
```
###Create Admin User
Create a file named ecadmin.ldif with the following contents:
```shell
dn: uid=ecadmin,ou=People,dc=whonome,dc=com
uid: ecadmin
cn: EC Admin
sn: Admin
mail: ecadmin@whonome.com
objectClass: person
objectClass: organizationalPerson
objectClass: inetOrgPerson
objectClass: posixAccount
objectClass: top
objectClass: shadowAccount
givenName: Admin
displayName: EC Admin
uidNumber: 10003
gidNumber: 5000
userPassword: Whonome#1
gecos: EC Admin
loginShell: /bin/bash
homeDirectory: /home/ecadmin
```
</div>
