# Introduction

Cloud infrastructures have grown significantly in the past years delivering core compute, storage, network, and many other services. For organizations making the transition to the cloud with their applications or infrastructure deployment, it has become more challenging to manage the different aspects of Operations and Maintenance (O&M) especially addressing the security requirements and compliance in combination with DevOps processes. DevOps processes in the cloud environment has become more complicated and could cause a major bottleneck when dealing with a multitude of cloud solutions. The CI/CD pipeline is too complex leading to the management of the application lifecycle without tight integration and consistency. The result is applications tend to be tightly coupled to a specific cloud environment. Changes to every environment would require significant efforts. In addition, there could also be a lack of governance and visibility into the entire process.

There is a need to consider the Unified DevSecOps and Environment Orchestration Framework to improve the seamless integration, consistency and most importantly security compliance for the application and infrastructure deployment. The successful implementation of this approach will provide enhanced values for the organizations. This can be achieved with the following:

-	Provide an abstract layer for the orchestration of cloud environments and services
-	Allow the seamless integration, deployment, and migration to support many applications in each environment
-	Decouple DevSecOps processes from the cloud environment
-	Provide framework for repeatable processes


## Everest Conductor

Unified DevSecOps and Environment Orchestration Framework bring together the disparate aspects of cloud management and DevSecOps allowing project teams to focus on delivery.

Simply Put: Enhance security while reducing project complexity and development time.

Two key security concepts which make systems much more resilient also make the seamless integration from a developer perspective more difficult, and they must be included in any unified approach. These two factors are Zero-Trust principle and defense in depth. Additional factors for organizations to consider the following factors in adopting the Unified DevSecOps and Environment Orchestration Framework are:

-	Lead speed for deployment of information system
-	Enhanced security
-	Vendor neutrality
-	Enhanced Quality
-	Operational process improvement
-	Flexible solution
-	Enterprise system


## Zero-Trust Principle

The goal of a Zero-Trust principle is to minimize collateral damage when a system is compromised. Zero-Trust assumes it is not a question of if it happens, it is a matter of when it will happen. Therefore, there is no “inside the boundary”, assume everything is exposed and requires secure and authenticated connections. At the foundation of Zero-Trust are three concepts: explicit verification, least privilege, and assume breached systems.

Zero-Trust involves many separate components; all of which must by synchronized and integrated. The following list shows the major ones:

-	Identities: Who is accessing the system, requires strong authentication systems, well known and understood business rules around authorization.
-	Server to Server: What servers and/or systems can access the application? What data is exposed? Applications that are silos and fail to interact with other enterprise systems often have reduced value.
-	Devices: What devices are accessing the system. Should the system be internal only? Is there a public trust aspect that must be considered? Do we limit device types? 
-	Applications: What security models are required? Does security run in the client or also on the server? On the client gives a better user experience, on the server provides better protection.
-	Data: protecting data around the exterior of the system, to managing the data at the core. This requires data-driven protection schemes which need to classify and label data. Encrypt and restrict access based on organizational policies.
-	Infrastructure: manage it, make sure it is patched and configured correctly.
-	Telemetry: actively mine the logs and other information to detect attacks and anomalies, automatically block and flag risky behavior, and employ least privilege access principles.
-	Network: Devices, systems and users are not trusted just because they are on an internal network. Encrypt all internal communications, limit access by policy, and employ micro-segmentation and real-time threat detection.

## Defense In-Depth

Often called the castle approach because it mirrors the layered defenses of a medieval castle. To penetrate a castle, attackers are faced with the moat, gatehouses, drawbridges, barbicans, inner walls, bailey, etc.  Like a castle, with a Defense in Depth approach, if one mechanism fails, the other steps up immediately to thwart an attack. This multi-layered approach with intentional redundancies increases the security of a system as a whole and addresses many different attack vectors.

For this approach to work, each layer generally must be diverse or separate from the other. The most obvious example for many people is the use of multi-factor authentication. By utilizing both knowledge (username/password) and something the user has (Yubi Key, Smart Card, Phone…), an attacker must compromise two very different layers of defense. As the result, is it takes state level actors to generally defeat MFA.

A second real-world example, where defense-in-depth can be useful, is in the protection of data that transit between various server components in enterprise systems. If the firewall is compromised, and the data is sent in the clear, the data will immediately be compromised. If the channel is encrypted, then not only must the firewall be compromised, but also one end of the server to server component must all become compromised.

Other examples are deploying additional firewalls around the application or utilizing host-based firewalls on each server. Each layer makes it more difficult for any single failure to compromise the system or the data. 
Finally, depending on a single strong exterior defense, we call the M&M defense. A hard shell with a gooey interior ready to be taken.

## Lead Speed

With the ability to inherit majority of security controls for a high security ATO system, the framework should provide most of the heavy lifting of the implementation of security controls for the development team. This helps ensure project teams meet standard security requirements. This also allows the development team to focus on functionality and minimize significant repetitive infrastructure as code required for security.

The framework should adopt industry standards for many best practices, by offering predefined standards for many aspects of development, project teams spend less time creating standards. It also helps in the ability to allow developers to cross teams and become productive immediately.

The framework should provide standard CI/CD process that can be easily customized in managing the release of software and deploying into multiple environments.


## Enhanced Security

A framework will enhance security compliance in every layer of processes and technology stacks. First, by having predefined templates for DevSecOps processes that include support for SAST and DAST, the project teams can deliver the application with built-in security at the start. Second, by offering standard session management, authentication and RBAC functionality; API services are well protected. Third, at the network level, the framework should automatically include a WAF which focuses on preventing the OWASP vulnerabilities. In addition, project teams can also inherit a large amount of security controls required to meet the ATO compliance and maintain its enhanced security posture.

Each project will be unique based on various factors; however, all projects share a set of common security requirements in addition to the development and operational processes. The framework should be designed around a consistent security model, with SSO capabilities, session management, integrated DevSecOps processes and other associated tools. By allowing the project teams to focus on functionality, the organizations can be confident that core security principles are met, and project functionalities are delivered.

## Vendor Neutrality

The framework must have the ability to support the interoperability with many major CSP. It provides an abstract layer that integrates and orchestrates most specific services provided by each CSP. This allows organizations to utilize the framework with a standard approach to manage the heterogenous services including specific security, network, and services provided by different CSPs. As the results, organizations can easily switch the deployment to different CSP or combine into a hybrid solution utilizing services provided by different CSPs based on business requirements.

## Enhanced Quality

Quality relies on consistency, repeatability, and the related value from meeting these objectives. At the same time, there is an expectation that quality will continue to improve. To achieve this goal, both the organization and the system must support a repeatable process but be willing to improve and change as the needs are changed. While simple in concept, quality is often challenging to deliver.

When assessing quality of a solution, it can be viewed subjectively. As an example, the organizations may be concerned with lower costs in meeting both security and functional requirements. End users would be concerned with ease of use. And, project teams would only focus on the development and delivery. The quality of project will vary based on these different views. There must be an alignment of quality objectives among the stakeholders. 

## Operational Process Improvement

Operational process improvement is often a foundational requirement and continuous effort for any organization. The framework should help drive the operational efficiency by providing:

-	Centralized management of distributed system resources and services
-	Shared standard processes at every stage of the system life cycle
-	Repeatable processes simplifying how works get done and deliver intuitive experiences
-	Consistent and reliable configuration management
-	Centralized assess management
-	Improved integration and interoperability
-	Insights to performance information
-	Optimized handling of increasing workloads with minimum resources

## Flexible Solution

With an understanding of the rapid growth and changes in the technology landscape, any framework must be designed to have the agility and flexibility to transform while maintaining the existing security posture. The project teams may decide almost nothing predefined and roll out the complete custom solution. Or, the project team may accept many predefined industry best practices and as a result have minimal security work or custom processes to develop. The framework must be designed to help projects achieve security compliance with minimal disruption or hinderance to productivity.

Based on assessing a diverse group of projects, there are three major groups of projects, each with competing demands. One approach would be to allow each project team to determine the level of standards to which each project team would comply. The three major groups of projects included:

1.	Established projects which have unique, custom security and process flows.
2.	New Projects applying standard best practice.
3.	New projects utilizing leading edge technology and third-party services.

Many established legacy projects have not been upgraded to run within containers or designed to operate within the revised security requirement including Segregation of Duty (SOD) where developers may not have access to production environment. To facilitate these projects, the framework must be designed to allow the project teams to maintain the traditional level of control for which they are accustomed while adding the benefits from many supplemental services, such as patch management, network security, web application firewalls. Over time, the projects may fully transition to the framework controls and processes.
Projects which are already established to work with containers and utilize well established and known cloud services will find migration or adoption of the framework with ease. They will also save significant efforts on ATO compliance and operations costs.
For projects adopting leading edge offerings from cloud provider, the framework would offer the ability to selectively add custom extensions to the environment. This allows the project teams to inherit standard security controls for most of the application and minimize the custom ATO security controls that are required to be implemented.

## Enterprise System

It is the seamless integration of all these disparate services which remove the barriers from project teams to handle many common security tasks. It also enhances the overall security posture by offering a well-maintained common infrastructure that follows industry best practices.

Based on the Enterprise focus of the framework, many aspects or components are designed to integrate with existing infrastructure if present. The following services can be designed to allow integration with the existing infrastructure:

-	Central Log management and SIEM
-	Backup/Restore
-	External Scanners
-	Host based Scanners
-	Scanner Profiles (CIS or STIG)
-	Code Scanning
-	OS Standards
-	Anti-virus Solutions

## About Whonome, LTD

As a consulting firm with experts in analytics, digital, engineering, and cybersecurity, we help organizations transform. We play trusted roles on many innovative programs for the governments. We work closely with clients in choosing the right strategy and technology to help them achieving the mission. We lead in providing enterprise solutions, using innovative approach to deliver high-performing computing and cloud technologies. We help clients standardize across environments, develop cloud-native applications, and integrate, automate, secure, and manage complex environments with quality support, training, and consulting services.
