What's new in Everest Conductor
===============================

What's New in Everest Conductor 0.1
-----------------------------------

Cloud Platforms 
^^^^^^^^^^^^^^^

Everest Conductor 0.1 supports the following Cloud Platforms:

- Amazon AWS
- Microsoft Azure
- Google GCP

